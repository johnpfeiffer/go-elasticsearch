package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"strconv"
)

/*
docker pull elasticsearch:1.5.2
docker run --rm -it --publish 9200:9200 --publish 9300:9300 --name myelasticsearch elasticsearch:1.5.2
curl localhost:9200
https://www.elastic.co/guide/en/elasticsearch/reference/current/docs.html
*/

// MyUser example but beware case sensitivity and exported fields https://blog.john-pfeiffer.com/golang-json-is-challenging/
type MyUser struct {
	Name string
	Age  int `json:"Age"`
}

// ESVersion is the JSON returned by the default root URL (i.e. 127.0.0.1:9200)
type ESVersion struct {
	Status      int    `json:"status"`
	Name        string `json:"name"`
	ClusterName string `json:"cluster_name"`
	Version     struct {
		Number         string `json:"number"`
		BuildHash      string `json:"build_hash"`
		BuildTimestamp string `json:"build_timestamp"`
		BuildSnapshot  bool   `json:"build_snapshot"`
		LuceneVersion  string `json:"lucene_version"`
	} `json:"version"`
	TagLine string `json:"tagline"`
}

func main() {
	elasticsearchServer := flag.String("elasticsearch", "http://localhost:9200", "the elasticsearch server address and port")
	createData := flag.Bool("createdata", true, "whether to create test data")
	flag.Parse()
	// https://github.com/mattbaird/elastigo/blob/master/lib/connection.go
	c := elastigo.NewConn()
	c.SetFromUrl(*elasticsearchServer)
	fmt.Println("Connecting to", c.Domain, c.Port)
	defer c.Close()

	index := "example"
	if *createData {
		fmt.Println("Inserting documents (the same document id will overwrite with a new version)")
		// https://github.com/mattbaird/elastigo/blob/master/lib/coreindex.go
		_, err := c.Index(index, "user", "docid_2", nil, map[string]string{"Name": "bob"})
		exitIfError(err)
		// Since the struct is defined with capital letters and JSON is case sensitive
		_, err = c.Index(index, "user", "docid_1", nil, `{"Name":"alice", "Age": 64}`)
		exitIfError(err)
		_, err = c.Index(index, "user", "docid_3", nil, MyUser{"charles", 64})
		exitIfError(err)
	}

	fmt.Println("getUserByID docid_2...")
	user := getUserByID(c, index, "docid_2")
	fmt.Println(user)

	fmt.Println("Searching for users that are age 64...")
	users := getUsersByAge(c, index, 64)
	for _, u := range users {
		fmt.Println("  ", u)
	}

	fmt.Println("Listing indices...")
	indexNames := getIndices(c)

	for _, index := range indexNames {
		allUsers := GetAllDocuments(c, index)
		fmt.Println(index, allUsers)
	}

}

func getIndices(c *elastigo.Conn) []string {
	// https://github.com/mattbaird/elastigo/blob/master/lib/catindexinfo.go
	// curl localhost:9200/_aliases
	var names []string
	indices := c.GetCatIndexInfo("")
	plural := true
	switch {
	case len(indices) == 1:
		plural = false
		fallthrough
	case len(indices) > 0:
		if plural {
			fmt.Println(len(indices), "indices found")
		} else {
			fmt.Println("1 index found")
		}
		fmt.Println("Name        Health      Status      Docs")

	default:
		fmt.Println("no indices found")
	}
	if len(indices) > 1 {

	}

	for _, index := range indices {
		names = append(names, index.Name)
		fmt.Printf("%s    %s    %s      %d \n", index.Name, index.Health, index.Status, index.Docs.Count)
	}
	return names
}

// retrieve by document id, https://github.com/mattbaird/elastigo/blob/master/client.go
func getUserByID(c *elastigo.Conn, index string, id string) MyUser {
	var u MyUser
	response, _ := c.Get(index, "user", id, nil)
	// fmt.Printf("%#v \n", response)
	bytes, err := response.Source.MarshalJSON()
	exitIfError(err)
	err = json.Unmarshal(bytes, &u)
	exitIfError(err)
	return u
}

// https://github.com/mattbaird/elastigo/blob/master/lib/coresearch.go
func getUsersByAge(c *elastigo.Conn, index string, age int) []MyUser {
	var users []MyUser
	searchJSON := `{
	    "query" : {
	        "term" : { "Age" : ` + strconv.Itoa(age) + ` }
	    }
	}`
	result, err := c.Search(index, "user", nil, searchJSON)
	exitIfError(err)
	// fmt.Printf("%#v \n", result)

	fmt.Println("getUsersByAge", age, "results count:", result.Hits.Len())
	if result.Hits.Len() > 0 {
		for _, hit := range result.Hits.Hits {
			var u MyUser
			bytes, err := json.Marshal(hit.Source)
			exitIfError(err)
			err = json.Unmarshal(bytes, &u)
			exitIfError(err)
			users = append(users, u)
		}
	}
	return users
}

// GetAllDocuments retrieves all of the JSON strings in an index
// https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-all-query.html
// http://localhost:9200/foo/_search?pretty=true&q=*:*
func GetAllDocuments(c *elastigo.Conn, index string) []string {
	var docs []string
	searchJSON := `{
	    "query" : {
	        "match_all": {}
	    }
	}`
	result, err := c.Search(index, "", nil, searchJSON)
	exitIfError(err)
	fmt.Println(index, "documents count:", result.Hits.Len())
	if result.Hits.Len() > 0 {
		for _, hit := range result.Hits.Hits {
			bytes, err := json.Marshal(hit.Source)
			data := fmt.Sprintf("%#v ", hit)
			warnIfError(data, err)
			docs = append(docs, string(bytes))
		}
	}
	return docs
}

func exitIfError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func warnIfError(data string, err error) {
	if err != nil {
		fmt.Println("WARNING could not parse with error:", err, "DATA:", data)
	}
}
